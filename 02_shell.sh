#!/bin/bash
echo "alias vpn-on='sudo openconnect -b --protocol=gp remote.bcchr.ca'" >> ~/.zshrc
echo "alias vpn-off='sudo killall -SIGINT openconnect'" >> ~/.zshrc
echo "export PATH=/usr/local/opt/php@7.4/bin:$PATH" >> ~/.zshrc