#!/bin/bash

# Setup homebrew (x86_64 installation)
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
# This line is required for M1 support, but won't break Intel-based
export PATH="$PATH:/opt/homebrew/bin"

# Install all the things
brew install openconnect \
             php@7.4 \
             drud/ddev/ddev \
             node

# Install nativefier via npm
npm install -g nativefier

# Install all the things, cask edition  
brew install --cask docker \
             sequel-pro \
             harvest \
             appcleaner \
             1password \
             alfred \
             firefox \
             hyper \
             visual-studio-code \
             slack \
             jetbrains-toolbox

# Following line may need adjustment for M1 based machines
export PATH="/usr/local/opt/php@7.4/bin:$PATH"
ddev config global --mutagen-enabled

# Install Composer (use OSX prebuilt php)
cd ~/
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --filename=composer --install-dir=/usr/local/bin
rm composer-setup.php

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name "Mark Labrecque"
git config --global user.email mark@affinitybridge.com

# Install vim and oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"